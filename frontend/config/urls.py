from django.conf.urls import patterns, include, url

from django.contrib import admin
from main.views import show_homepage
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
    url(r'', show_homepage, name='demo_discretebarchart'),
)
