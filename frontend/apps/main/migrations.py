from django.db import connections
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import ConnectionDoesNotExist

from main.models import Person

"""
    migrations.py was included to import from the backend sqlite3 database.
    This script is ran when the server is started.

    If I was building to production standards I would have set up an https
    connection between the front- and backend.

"""


def setup_cursor():
    try:
        cursor = connections['backend'].cursor()
        return cursor
    except ConnectionDoesNotExist:
        print "Legacy database is not configured"
        return None


def import_people_person():

    cursor = setup_cursor()
    if cursor is None:
        return

# The simple sql command for the people_person table.
    sql = """SELECT id, name, car_make FROM people_person"""
    cursor.execute(sql)

# Save the table on the frontend.
    for row in cursor.fetchall():
        person = Person(id=row[0], name=row[1], car_make=row[2])
        person.save()


def main():
    import_people_person()

if __name__ == '__main__':
    main()
