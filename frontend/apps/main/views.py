from django.http import HttpResponse
from django.shortcuts import render_to_response
import random
import datetime
import time

from main.models import Person

"""
    This is basically the demo page for the django-nvd3 package.  The first
    package (django-report-tools) I used was apparently outdated because it
    would not render the page.  It is actually a great package and ought to
    be fixed by someone familiar with JS; very intuitive and clean code to
    build the plot.

    The modification I made to the demo code was to the x- and ydata.  The
    first block sorts through the data model to get the information we want.
"""


def show_homepage(request):
    """
    discretebarchart page
    """

    table = Person.objects.using('backend').all()

    prev_make = table[0].car_make
    count     = 0
    car_makes = []
    car_count = []

    car_makes.append(table[0].car_make)

    for entry in table:
        if entry.car_make != prev_make:
            car_count.append(count)
            car_makes.append(entry.car_make)

            prev_make = entry.car_make
            count     = 1

        else:
            count += 1

    car_count.append(count)

#    extra_serie1 = {"tooltip": {"y_start": "", "y_end": " cal"}}
    chartdata = {
        'x': car_makes, 'name1': 'Car Make', 'y1': car_count,
    }
    charttype = "discreteBarChart"
    data = {
        'charttype': charttype,
        'chartdata': chartdata,
        'chartcontainer': 'discretebarchartcontainer_container',
        'extra': {
            'x_is_date': False,
            'x_axis_format': '',
            'tag_script_js': True,
            'jquery_on_ready': True,
        },
    }
    return render_to_response('main/discretebarchart.html', data)
