from django.db import models


class Person(models.Model):
    # TODO: Define model to match database
    id       = models.IntegerField(primary_key=True)
    name     = models.CharField(max_length=200, blank=True)
    car_make = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return self.car_make
